import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Box, Typography, Fab } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import { HashtagSolid } from './components/Icons';

const styles = theme => ({
    backFab: {
        color: '#B01925',
        backgroundColor: '#white',
        marginLeft: '10px',
    },
    addFlex:{
        display: 'flex',
    },
    statusBox:{
        border: '1px solid #299808',
        borderRadius: '11px',
        margin: 'auto',
    },
    statusText:{
        fontSize: '13px',
        margin: 'auto',
    },
    colorGreen:{
        color: '#299808'
    },
    colorBlue:{
        color: '#024CB1'
    },
    colorRed:{
        color: '#B01925',   
    },
    colorWhite:{
        color: '#FFFFFF'
    }
});
  
class PropertyStatus extends Component {
    constructor(props){
        super(props)
        this.state = {}
    }
    render() {
        const { classes } = this.props;
        return (
            <Box className={classes.addFlex} component="span">
                <Fab size="small" className={classes.backFab}>
                    <ArrowBackIcon />
                </Fab>
                <Box className={`${classes.statusBox} ${classes.addFlex}`} ml={1} px={2} >
                    <Typography         className={`${classes.statusText} ${classes.colorGreen}`} variant="h6" >Published</Typography>
                    <ArrowDropDownIcon  className={`${classes.statusText} ${classes.colorRed}`} style={{fontSize:'37px'}} />
                    <HashtagSolid       className={`${classes.statusText} ${classes.colorRed}`} style={{fontSize:'12px', marginRight: '5px'}} />
                    <Typography         className={`${classes.statusText} ${classes.colorBlue}`} variant="h6" >Ref No.</Typography>
                    <Typography         className={`${classes.statusText} ${classes.colorWhite}`} variant="h6" >WI956709</Typography>
                </Box>
            </Box>
        )
    }
}

PropertyStatus.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles)(PropertyStatus);