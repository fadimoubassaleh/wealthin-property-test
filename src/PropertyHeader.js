import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Box, Typography, Fab, SvgIcon, Chip } from '@material-ui/core';
import { EditSolid } from './components/Icons';

const SaleChip = withStyles({
    root: {
        backgroundColor: '#B01925',
        color: '#FFFFFF',
        margin: 'auto 15px auto 0',
    },
    label: {
        padding: '0 18px',
    }
})(Chip);

const HeaderText = withStyles({
    root: {
        fontSize: '20px',
        fontWeight: 'bold',
        color: '#ffffff',
        marginTop: 'auto',
        marginBottom: 'auto',
    }
})(Typography);
    
class PropertyHeader extends Component {
    render(){
        return(
            <Box>
                <Grid container>
                    <Grid item container lg={9}>
                        <SaleChip size="small" label="Sale"/>
                        <HeaderText>2 Bedroom - Apartment - Marina Residences, Trunk West - Palm Jumeirah</HeaderText>
                    </Grid>
                    <Grid item container lg={3} justify={'flex-end'}>
                        <HeaderText style={{marginRight: '10px'}}>AED</HeaderText>
                        <HeaderText>1,749,000</HeaderText>
                        <Fab size="small" style={{marginLeft: '10px'}}>
                            <EditSolid fontSize={'small'} htmlColor={'#024CB1'}/>
                        </Fab>
                    </Grid>
                </Grid>
            </Box>
        )
    }
}

export default PropertyHeader;