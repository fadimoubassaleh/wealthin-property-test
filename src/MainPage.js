import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Box, Divider, Container } from '@material-ui/core';
import TabsSection from './TabsSection';
import PropertyStatus from './PropertyStatus';
import PropertyHeader from './PropertyHeader';
import PropertyDetailsTab from './tabs/PropertyDetailsTab';
import OffersTab from './tabs/OffersTab';
import MarketReportTab from './tabs/MarketReportTab';
import './App.scss';

const CustBox = withStyles({
    root: {
        backgroundColor: '#22273F',
        boxShadow: '0px 3px 6px #00000029',
        padding: '26px 62px',
        borderRadius: '10px',
        height: '100%',
    },
})(Box);

const CustDivider = withStyles({
    root: {
        backgroundColor: '#707070',
        marginTop: '9px',
        marginBottom: '30px',
    },
})(Divider);

class MainPage extends Component {
    constructor(){
        super()
        this.state = {
            the_tab: 0
        }
    }
    tabChange = (newValue) => {
        this.setState({the_tab: newValue});
    };
    render() {
        return (
            <Container maxWidth="xl" className="main-container">
                <Grid container className="the-head">
                    <Grid item lg={3}>
                        <PropertyStatus/>
                    </Grid>
                    <Grid item lg={9}>
                        <TabsSection the_tab={this.state.the_tab} tabChange={this.tabChange} />
                    </Grid>
                </Grid>
                <CustBox>
                    <PropertyHeader/>
                    <CustDivider />
                    <PropertyDetailsTab tabValue={this.state.the_tab} index={0}/>
                    <OffersTab tabValue={this.state.the_tab} index={1}/>
                    <MarketReportTab tabValue={this.state.the_tab} index={2}/>

                </CustBox>
            </Container>
        )
    }
}
export default MainPage;