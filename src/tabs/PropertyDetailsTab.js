import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Typography, Divider, Box, Button, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@material-ui/core';
import { CompressSolid, BuildingSolid, Bed, CouchSolid, SnowflakeSolid, RulerVerticalSolid, BathtubWithOpenedShower, VectorSquareSolid, ParkingSolid, MoneyBillWaveSolid } from '../components/Icons';
import TabPanel from '../components/TabPanel';
import SingleStatus from '../components/SingleStatus';
import PropertyMedia from '../components/PropertyMedia';

const styles = theme => ({
    text: {
        color: '#ffffff',
    },
    textRow: {
        margin: '25px',
    },
    dividerInside:{
        backgroundColor: '#D3D3D3',
        marginTop: '7px'
    },
    boldText: {
        fontWeight: 'bold'
    },
    marginBottom: {
        marginBottom: '20px'
    },
    marginLeft: {
        marginLeft: '7px'
    },
    marginTop:{
        marginTop: '20px'
    },
    boxPadding: {
        padding: '40px 20px'
    },
    boxStyle: {
        padding: '40px 25px',
        backgroundColor: '#22273F',
        boxShadow: '0px 0px 12px #111320',
        borderRadius: '8px',
    }
});


class PropertyDetailsTab extends Component {
    constructor(props){
        super(props)
        this.state = {
            theDetails:[
                {
                    icon: BuildingSolid,
                    title: 'Type',
                    details: 'Apartment',
                },
                {
                    icon: RulerVerticalSolid,
                    title: 'Height',
                    details: '47',
                },
                {
                    icon: Bed,
                    title: 'Bedrooms',
                    details: '2',
                },
                {
                    icon: BathtubWithOpenedShower,
                    title: 'Bathrooms',
                    details: '3',
                },
                {
                    icon: BuildingSolid,
                    title: 'Built-Up',
                    details: '1800.00',
                },
                {
                    icon: VectorSquareSolid,
                    title: 'Plot Size',
                    details: '0',
                },
                {
                    icon: CouchSolid,
                    title: 'Furnished',
                    details: 'furnished',
                },
                {
                    icon: ParkingSolid,
                    title: 'Parking',
                    details: '2',
                },
                {
                    icon: SnowflakeSolid,
                    title: 'Cooling',
                    details: 'Empower',
                },
                {
                    icon: MoneyBillWaveSolid,
                    title: 'Service Charge',
                    details: 'AED 15 per.Sqft',
                },
            ],
            dialogOpen: false
        }
    }

  dialogHandleClickOpen  = () => {
    this.setState({dialogOpen: true})
  };

  dialogHandleClose = () => {
    this.setState({dialogOpen: false})
  };
    render(){
        const { classes } = this.props;
        return(
            <TabPanel className="tab-panel-container" value={this.props.tabValue} index={this.props.index}>
                <Grid container spacing={4}>
                    <Grid item lg={3}>
                        <PropertyMedia />
                    </Grid>
                    <Grid item container lg={6}>
                        <Grid item lg={12}>
                            <Typography className={`${classes.text} ${classes.boldText}`} >Property Details</Typography>
                            <Divider className={classes.dividerInside}/>
                        </Grid>
                        <Grid item container lg={12} className={classes.boxPadding}>
                            {this.state.theDetails.map((value, i) => (
                                <Grid key={i} item container lg={6} className={classes.marginBottom}>
                                    <Grid item container lg={7}>
                                        <value.icon fontSize={'small'} htmlColor={'#D5408F'} />
                                        <Typography className={`${classes.text} ${classes.boldText} ${classes.marginLeft}`} >{value.title}</Typography>
                                    </Grid>
                                    <Grid item lg={5}>
                                        <Typography className={classes.text} >{value.details}</Typography>
                                    </Grid>
                                </Grid>
                            ))}
                        </Grid>
                        <Grid container justify="flex-end">
                            <Button onClick={this.dialogHandleClickOpen} style={{color: "#B01925"}}>More Details <CompressSolid className={classes.marginLeft} fontSize={'small'} htmlColor={'#B01925'}/></Button>
                            <Dialog
                                open={this.state.dialogOpen}
                                onClose={this.dialogHandleClose}
                                aria-labelledby="alert-dialog-title"
                                aria-describedby="alert-dialog-description"
                            >
                                <DialogTitle id="alert-dialog-title">{"Dialog Title!"}</DialogTitle>
                                <DialogContent>
                                    <DialogContentText id="alert-dialog-description">
                                        Dialog Body
                                    </DialogContentText>
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={this.dialogHandleClose} color="primary" autoFocus>
                                        Close
                                    </Button>
                                </DialogActions>
                            </Dialog>
                        </Grid>
                        <Grid item lg={12}>
                            <Typography className={`${classes.text} ${classes.boldText}`} >Description</Typography>
                            <Divider className={classes.dividerInside}/>
                            <Typography className={`${classes.text} ${classes.marginTop}`} >Spectacular amounts of Sea View,.High quality spacious Apartment More value for money.</Typography>
                            <Typography className={classes.text} >Close to a community center which has retail and other facilities.</Typography>
                            <Typography className={classes.text} >Gated, family-friendly, suburban community.</Typography>
                            <Typography className={classes.text} >Close to school</Typography>
                        </Grid>
                    </Grid>
                    <Grid item lg={3} >
                        <Box className={`${classes.boxStyle}`}>
                            <SingleStatus title={'Permit No.'} details={'5435436'}/>
                            <SingleStatus title={'Listing Expiry'} details={'9 jun 2021'}/>
                            <Divider className={classes.dividerInside}/>
                            <SingleStatus title={'DEWA Premises No.'} details={'383-10689-3'}/>
                            <SingleStatus title={'Makani Number'} details={'15866 77079'}/>
                            <Divider className={classes.dividerInside}/>
                            <SingleStatus title={'Rental Status'} details={'Vacant'}/>
                            <SingleStatus title={'Completion Staus'} details={'Ready / Completed'}/>
                        </Box>
                    </Grid>
                </Grid>
            </TabPanel>
        )
    }
}

PropertyDetailsTab.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles)(PropertyDetailsTab);