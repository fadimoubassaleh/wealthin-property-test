import React, { Component } from 'react';
import TabPanel from '../components/TabPanel';
    
class MarketReportTab extends Component {
    constructor(props){
        super(props)
        this.state = {}
    }
    render(){
        return(
            <TabPanel className="tab-panel-container" value={this.props.tabValue} index={this.props.index}>
                MarketReportTab
            </TabPanel>
        )
    }
}

export default MarketReportTab;