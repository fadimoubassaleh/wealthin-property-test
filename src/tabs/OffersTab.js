import React, { Component } from 'react';
import TabPanel from '../components/TabPanel';
    
class OffersTab extends Component {
    constructor(props){
        super(props)
        this.state = {}
    }
    render(){
        return(
            <TabPanel className="tab-panel-container" value={this.props.tabValue} index={this.props.index}>
                OffersTab
            </TabPanel>
        )
    }
}

export default OffersTab;