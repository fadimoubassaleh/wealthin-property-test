import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Typography, Box } from '@material-ui/core';


const styles = theme => ({
    text: {
        color: '#ffffff',
    },
    textRow: {
        margin: '25px',
    },
    boldText: {
        fontWeight: 'bold'
    }
});

class SingleStatus extends Component {
    constructor(props){
        super(props)
        this.state = {
        }
    }
    render(){
        const { classes } = this.props;
        return(
            <Box className={classes.textRow}>
                <Grid container>
                    <Grid item lg={7}>
                        <Typography className={`${classes.text} ${classes.boldText}`} >{this.props.title}</Typography>
                    </Grid>
                    <Grid item lg={5}>
                        <Typography className={classes.text} >{this.props.details}</Typography>
                    </Grid>
                </Grid>
            </Box>
        )
    }
}

SingleStatus.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles)(SingleStatus);