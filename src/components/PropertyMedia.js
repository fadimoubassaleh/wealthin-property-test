import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Box, Tabs, Tab, Fab } from '@material-ui/core';
import { All360Degree, LayerGroupSolid, ImagesSolid } from './Icons';
import TabPanel from './TabPanel';
import { EditSolid } from './Icons';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

const CustTabs = withStyles((theme) => ({
    flexContainer: {
        margin: '3px 3px 0px 3px',
    },
    indicator: {
        display: 'none',
        backgroundColor: 'transparent',
    },
    scrollButtons:{
        color: '#F2F2F2',
    }
}))(Tabs);
  
const CustTab = withStyles((theme) => ({
    root: {
        backgroundColor: '#22273F',
        color: '#F2F2F2',
        padding: 0,
        minWidth: '125px',
        fontSize: '14px',
        fontWeight: 'bold',
        boxShadow: '0px 3px 6px #000000',
        borderRadius: '10px 10px 0px 0px',
        textTransform: 'none',
    },
    selected: {
        background: '#FFFFFF',
        color: '#121632',
    },
}))((props) => <Tab {...props} />);
  
const styles = theme => ({
    boxStyle: {
        backgroundColor: '#22273F',
        boxShadow: '0px 0px 12px #111320',
        borderRadius: '0 0 8px 8px',
        margin: '0 3px',
        minHeight: '100px',
    },
    positionRelative: {
        position: 'relative',
    },
    positionAbsoluteRight: {
        position: 'absolute',
        right: '15px',
        bottom: '60px'
    }
});

class PropertyMedia extends Component {
    constructor(props){
        super(props)
        this.state = {
            theTab: 111,
        }
    }
    tabChange = (newValue) => {
        this.setState({theTab: newValue});
    };
    render(){
        const { classes } = this.props;
        return(
            <Box>
                <CustTabs 
                    value={this.state.theTab}
                    centered
                    variant="fullWidth">
                    <CustTab label={<span style={{display: 'flex'}}><ImagesSolid style={{marginRight: '5px'}} fontSize={'small'} htmlColor={'#D5408F'} />Photos</span>} onClick={()=>this.tabChange(111)}/>
                    <CustTab label={<span style={{display: 'flex'}}><LayerGroupSolid style={{marginRight: '5px'}} fontSize={'small'} htmlColor={'#D5408F'} />Floor Plan</span>} onClick={()=>this.tabChange(222)}/>
                    <CustTab label={<span style={{display: 'flex'}}><All360Degree style={{marginRight: '5px'}} fontSize={'small'} htmlColor={'#D5408F'} />360 View</span>} onClick={()=>this.tabChange(333)}/>
                </CustTabs>
                <TabPanel className="tab-panel-container" value={this.state.theTab} index={111}>
                    <Box className={`${classes.boxStyle} ${classes.positionRelative}`}>
                        <Carousel 
                            showArrows={false}
                            showStatus={false}
                            showIndicators={false}
                            >
                            <div>
                                <img src="./assets/1.jpeg" />
                            </div>
                            <div>
                                <img src="./assets/2.jpeg" />
                            </div>
                            <div>
                                <img src="./assets/3.jpeg" />
                            </div>
                        </Carousel>
                        <Fab size="small" className={classes.positionAbsoluteRight}>
                            <EditSolid fontSize={'small'} htmlColor={'#024CB1'}/>
                        </Fab>
                    </Box>
                </TabPanel>
                <TabPanel className="tab-panel-container" value={this.state.theTab} index={222}>
                    Floor Plan
                </TabPanel>
                <TabPanel className="tab-panel-container" value={this.state.theTab} index={333}>
                    360 View
                </TabPanel>
            </Box>
        )
    }
}

PropertyMedia.propTypes = {
    classes: PropTypes.object.isRequired,
};
  
export default withStyles(styles)(PropertyMedia);