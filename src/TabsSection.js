import React, { Component } from 'react';
import { Box, Tabs, Tab, Avatar } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const AntAvatar = withStyles({
    root:{
        display: 'flex',
        height: '20px',
        width: '20px',
        fontSize: '11px',
        marginRight: '6px',
        backgroundColor: '#B01925',
    }
})(Avatar);

const CustTabs = withStyles({
    indicator: {
        display: 'none',
        backgroundColor: 'transparent',
    },
    scrollButtons:{
        color: '#F2F2F2',
    }
  })(Tabs);
  
const CustTab = withStyles((theme) => ({
    root: {
        backgroundColor: '#22273F',
        color: '#F2F2F2',
        marginRight: '13px',
        fontWeight: 'bold',
        boxShadow: '0px 3px 6px #00000029',
        borderRadius: '10px 10px 0px 0px',
        textTransform: 'none',
    },
    selected: {
        background: '#FFFFFF',
        color: '#121632',
    },
}))((props) => <Tab {...props} />);
  
class TabsSection extends Component {
    
    constructor(props){
        super(props)
        this.state = {}
    }
    render() {
        return (
            <Box>
                <CustTabs 
                    value={this.props.the_tab}
                    variant="scrollable"
                    scrollButtons="auto">
                    <CustTab label="Property Details" onClick={()=>this.props.tabChange(0)} style={{marginLeft: 'auto'}}/>
                    <CustTab label={<span style={{display: "flex"}}><AntAvatar>0</AntAvatar>Offers</span>} onClick={()=>this.props.tabChange(1)} />
                    <CustTab label="Listing Statistics" onClick={()=>this.props.tabChange(2)}/>
                    <CustTab label="Market Report" onClick={()=>this.props.tabChange(3)}/>
                    <CustTab label="Document" onClick={()=>this.props.tabChange(4)}/>
                    <CustTab label="Calendar" onClick={()=>this.props.tabChange(5)}/>
                    <CustTab label="History" onClick={()=>this.props.tabChange(6)}/>
                </CustTabs>
            </Box>
        )
    }
}
export default TabsSection;